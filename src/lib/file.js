// var Parser = new Org.Parser();
// var Doc;

function getDocument(callback){
    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", function(resp){
        parse(oReq.response)
    });
    oReq.open("GET", "./file.org");
    oReq.send();
}

export let fetch = function(){
    return new Promise(function(done, err){
        let oReq = new XMLHttpRequest();
        oReq.addEventListener("load", function(resp){
            done(oReq.response)
        });
        oReq.open("GET", "./file.org");
        oReq.send();
    });
}

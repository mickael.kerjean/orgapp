import * as Org from 'org';
import { fetch } from './file';
import { ConverterOrg } from './orgExporter';

function Documents(){
    //this.config = new Config();
    this.documents = [];
}
export let Docs = new Documents();

function Config(){
    this.status = [],
    this.capture = []
}

function OrgDoc(content){
    this.content = null;
    this.todos = null;

    // hydrate object
    this.refresh(content);
}

function Todo(){
    this.title = null;
    this.description = ""
    this.status = "TODO";
    this.scheduled = null;
    this.tags = [];
    this.clocks = [];
    this.checklists = [];    
}
Todo.prototype.isValid = function(){
    if(this.title) return true;
}


Config.prototype.set = function(key, obj){
    if(key.indexOf['status', 'capture'] > 0) this.config[key] = obj;
    else throw "unknown configuration key"
}


Documents.prototype.add = function(filename){
    let self = this;
    return fetch(filename)
        .then(function(content){
            let doc = new OrgDoc(content);
            self.documents.push({
                filename: filename,
                doc: doc
            });
            return Promise.resolve(doc);
        });
}
Documents.prototype.get = function(filename){
    for(let i = 0; i < this.documents.length; i++){
        if(this.documents[i]['filename'] == filename){
            return Promise.resolve(this.documents[i]['doc']);
        }
    }
    throw "document: "+filename+" does not exist";
}


OrgDoc.prototype.refresh = function(content){    
    this.content = content;
    this.todos = _findTodos(content);
    this.nodes = _findTodos2(content);

    console.log(this)
}
OrgDoc.prototype.toString = function(){
    return this.content;
}



function _findTodos(content){
    let todos = [];
    let reg_todo_title = /^[\*]{1,}\s[A-Z]{4,} (.*)$/;
    let reg_todo_keyword = /^[\*]{1,}\s([A-Z]{4,})\s.*$/;
    let reg_checklist = /^\-\s\[\s\]\s(.*)$/;
    let reg_headline = /^[\*]{1,} (.*)$/;
    let todo = new Todo();
    
    content
        .split("\n")
        .map(function(line){
            line = line.trim();
            // new todo start with a headline
            if(reg_headline.test(line)){
                if(todo.isValid()){
                    todos.push(todo);
                }
                todo = new Todo();
                if(reg_todo_title.test(line) && reg_todo_keyword.test(line)){
                    todo.title = line.match(reg_todo_title)[1].trim();
                    todo.status = line.match(reg_todo_keyword)[1].trim();
                }
            }else if(todo.isValid()){ // update todo state if needed
                todo.description += line + "\n"
                // let checklist_match = line.match(/^\-\s\[\s\]\s(.*)$/);
                // if(checklist_match && checlist_match.length === 2){
                //     todo.checklist_add()
                // }
            }
        });

    return todos;
}

function _findTodos2(content){
    let Parser = new Org.Parser();
    let struct = Parser.parse(content);
    return struct;

    console.log(struct.nodes)
    //parseNodes(struct.nodes[0]);
    return struct.nodes.map(function(node, i){
        console.log("=> "+i)
        parseNodes(node);
    });
    
    function parseNodes(current_node, todos = [], todo = {}){
        debug(current_node);
        todoObj(current_node, todo, todos);
        
        if(current_node.children.length > 0){            
            current_node.children.map(function(next_node){
                parseNodes(next_node)
            });
        }
    }

    function todoObj(node, tmp_todo, all_todos){
        let reg_todo = /^[A-Z]{4,} (.*)$/;
        if(node.type === "text"){
            if(reg_todo.test(node.value)){
                if(tmp_todo.title) all_todos.push(tmp_todo);
                tmp_todo = {};            
            }
        }
    }

    function debug(node){
        let text = "";
        if(node.level) text += "level: "+node.level +"\t"
        if(node.fromLineNumber) text += "line: "+node.fromLineNumber+"\t";
        if(node.type) text += "type: "+node.type+"\t"
        if(node.value) text += "value: "+node.value
        console.log(text)
    }
}

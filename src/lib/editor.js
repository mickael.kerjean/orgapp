//import * as Ace from 'ace-builds/src/ace'
let ace = window.ace;

export function Editor(){
    var editor = ace.edit("editor");
    editor.setTheme("ace/theme/twilight");
    editor.session.setMode("ace/mode/markdown");

    return {
        getText: GetText.bind(null, editor),
        putText: PutText.bind(null, editor)
    }
}

function GetText(editor){
    return editor.getValue()
}

function PutText(editor, content){   
    editor.setValue(content);
}

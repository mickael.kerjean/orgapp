import { Editor } from './lib/editor';
import { Docs } from './lib/orgmode';
document.addEventListener("DOMContentLoaded", $init);

const FILENAME = "file.org"

function $init(){
    // initialize dom
    let $dom = {
        save: document.querySelector("#fab")        
    };   
    let editor = Editor();
    $dom.save.addEventListener('click', save.bind(null, editor));
    
    Docs.add(FILENAME).then(function(doc){
        editor.putText(doc.content);
    });
}

function save(editor){
    let content = editor.getText();
    Docs.get(FILENAME).then(function(doc){
        doc.refresh(content);
        // console.log(doc)
        // console.log(doc.toString())
    });
}
